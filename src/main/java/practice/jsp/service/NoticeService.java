package practice.jsp.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import practice.jsp.model.Notice;
import practice.jsp.model.NoticeCommentCountView;

//Notice를 전문으로 CRUD, 비즈니스 로직을 처리하는 서비스
public class NoticeService {
	private static final int COUNT_OF_NOTICE_ON_PAGE = 10;
	
	private static NoticeService instance = null;

	private DataSource dataFactory;

	private NoticeService() {
		// tomcat에 설정한 connectionPool 사용하는 방법
		Context ctx;
		try {
			ctx = new InitialContext();
			Context cnvContext = (Context) ctx.lookup("java:/comp/env");
			dataFactory = (DataSource) cnvContext.lookup("jdbc/mariadb");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	};

	public static NoticeService getInstance() {
		if (instance == null) {
			synchronized (NoticeService.class) {
				if (instance == null) {
					instance = new NoticeService();
				}
			}
		}
		return instance;
	}
	
	public int createNotice(Notice notice) {
		int result = -1;
		
		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = dataFactory.getConnection();
			
			String title = notice.getTitle();
			String writerId = notice.getWriterId();
			String content = notice.getContent();
			String files = notice.getFiles();
			boolean pub = notice.isPub();
			
			String sql = "INSERT INTO notice "
					+ "(TITLE, WRITER_ID, CONTENT, FILES, PUB) "
					+ "VALUES(?, ?, ?, ?, ?)";
			
			// ?로 나중에 값을 넣을 때는 prepareStatement를 사용하면 된다.
			st = conn.prepareStatement(sql);
			st.setString(1, title);
			st.setString(2, writerId);
			st.setString(3, content);
			st.setString(4, files);
			st.setBoolean(5, pub);
			
			// 변경된 row의 수를 반환한다.
			result = st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	public int updateNotice(Notice notice) {
		return 0;
	}
	
	public int removeNotice(int id) {
		return 0;
	}
	
	public int pubNoticeAll(int[] openIds, int[] closeIds) {
		List<String> openIdList = openIds == null
				? new ArrayList<String>()
				: Arrays.stream(openIds).mapToObj(String::valueOf)
					.collect(Collectors.toList());
		
		List<String> closeIdList = closeIds == null
				? new ArrayList<String>()
				: Arrays.stream(closeIds).mapToObj(String::valueOf)
					.collect(Collectors.toList());
		
		return pubNoticeAll(openIdList, closeIdList);
	}
	
	public int pubNoticeAll(List<String> openIdList, List<String> closeIdList) {
		String openIdsCSV = String.join(",", openIdList);
		String closeIdsCSV = String.join(",", closeIdList);
		
		return pubNoticeAll(openIdsCSV, closeIdsCSV);
	}
	
	public int pubNoticeAll(String openIdsCSV, String closeIdsCSV) {
		int result = 0;
		
		Connection conn = null;
		Statement st = null;
		try {
			conn = dataFactory.getConnection();
			conn.setAutoCommit(false);
			
			String closeSql = "UPDATE notice "
					+ "SET "
					+ "PUB=0 "
					+ "WHERE "
					+ "ID IN (" + closeIdsCSV + ")";
			st = conn.createStatement();
			result = st.executeUpdate(closeSql);
			
			String openSql = "UPDATE notice "
					+ "SET "
					+ "PUB=1 "
					+ "WHERE "
					+ "ID IN (" + openIdsCSV + ")";
			result += st.executeUpdate(openSql);
			
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				if (conn != null) {
					conn.rollback();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}				
			
		} finally {
			try {
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	public int removeNoticeAll(int[] ids) {
		int result = 0;
		
		Connection conn = null;
		Statement st = null;
		try {
			conn = dataFactory.getConnection();
			
			// int[]형 배열을 ,단위로 조인해서 String형으로 변환
			String strIds = Arrays.stream(ids)
					.mapToObj(String::valueOf)
					.collect(Collectors.joining(","));
			
			String sql = "DELETE FROM notice "
					+ "WHERE "
					+ "ID IN (" + strIds + ")";
			st = conn.createStatement();
			result = st.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	public List<NoticeCommentCountView> getLatestNoticeList() {
		return null;
	}
	
	public List<NoticeCommentCountView> getPubNoticeList() {
		return getPubNoticeList(1, null, null);
	}
	
	public List<NoticeCommentCountView> getPubNoticeList(int page) {
		return getPubNoticeList(page, null, null);
	}
	
	public List<NoticeCommentCountView> getPubNoticeList(String field, String query) {
		return getPubNoticeList(1, field, query);
	}
	
	public List<NoticeCommentCountView> getPubNoticeList(int page, String field, String query) {
		List<NoticeCommentCountView> notices = new ArrayList<NoticeCommentCountView>();
		
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			conn = dataFactory.getConnection();
			
			if(page < 1) {
				page = 1;
			}
			int offset = (page - 1) * COUNT_OF_NOTICE_ON_PAGE;
			
			switch(field == null ? "" : field) {
				case "title":
				case "content":
				case "writer_id":
				{
					String sql = "SELECT * "
							+ "FROM notice_comment_count_view "
							+ "WHERE "
							+ "PUB "
							+ "AND" + field + " LIKE ? "
							+ "ORDER BY REGDATE DESC "
							+ "LIMIT ?, ?";
					st = conn.prepareStatement(sql);
					st.setString(1, "%" + query + "%");
					st.setInt(2, offset);
					st.setInt(3, COUNT_OF_NOTICE_ON_PAGE);
					break;
				}
				default:
				{
					String sql = "SELECT * "
							+ "FROM notice_comment_count_view "
							+ "WHERE "
							+ "PUB "
							+ "ORDER BY REGDATE DESC "
							+ "LIMIT ?, ?";
					st = conn.prepareStatement(sql);
					st.setInt(1, offset);
					st.setInt(2, COUNT_OF_NOTICE_ON_PAGE);
					break;
				}
			}
			rs = st.executeQuery();
			
			while(rs.next()) {
				int id = rs.getInt("ID");
				String title = rs.getString("TITLE");
				String writerId = rs.getString("WRITER_ID");
				Timestamp regDate = rs.getTimestamp("REGDATE");
				int hit = rs.getInt("HIT");
				String files = rs.getString("FILES");
				boolean pub = rs.getBoolean("PUB");
				int commentCount = rs.getInt("COMMENT_COUNT");
				
				notices.add(new NoticeCommentCountView(id, title, writerId, regDate, hit, files, pub, commentCount));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) {
					rs.close();
					rs = null;
				}
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return notices;
	}
	
	public int getPubNoticeCount(String field, String query) {
		int result = 0;
		
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			conn = dataFactory.getConnection();
			
			switch(field == null ? "" : field) {
				case "title":
				case "content":
				{
					String sql = "SELECT COUNT(*) FROM notice "
							+ "WHERE "
							+ "PUB "
							+ "AND " + field + " LIKE ? ";
					st = conn.prepareStatement(sql);
					st.setString(1, "%" + query + "%");
					break;
				}
				default:
				{
					String sql = "SELECT COUNT(*) FROM notice "
							+ "WHERE "
							+ "PUB";
					st = conn.prepareStatement(sql);
					break;
				}
			}
			rs = st.executeQuery();
			
			if(rs.next()) {
				int count = rs.getInt(1);
				
				result = count;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) {
					rs.close();
					rs = null;
				}
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	public int getPubNoticeCount() {
		return getPubNoticeCount(null, null);
	}
	
	public Notice getPubNotice(int id) {
		Notice notice = null;
		
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			conn = dataFactory.getConnection();
			
			String sql = "SELECT * FROM notice "
					+ "WHERE "
					+ "PUB "
					+ "AND ID=?";
			st = conn.prepareStatement(sql);
			st.setInt(1, id);
			rs = st.executeQuery();
			
			if(rs.next()) {
				String title = rs.getString("TITLE");
				String writerId = rs.getString("WRITER_ID");
				String content = rs.getString("CONTENT");
				Timestamp regDate = rs.getTimestamp("REGDATE");
				int hit = rs.getInt("HIT");
				String files = rs.getString("FILES");
				boolean pub = rs.getBoolean("PUB");
				
				notice = new Notice(id, title, writerId, content, regDate, hit, files, pub);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) {
					rs.close();
					rs = null;
				}
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return notice;
	}

	public Notice getPrevPubNotice(int id) {
		Notice notice = null;
		
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			conn = dataFactory.getConnection();
			
			String sql = "SELECT * FROM notice "
					+ "WHERE "
					+ "PUB "
					+ "AND REGDATE < (SELECT REGDATE FROM notice WHERE ID=?) "
					+ "ORDER BY REGDATE DESC LIMIT 1";
			st = conn.prepareStatement(sql);
			st.setInt(1, id);
			rs = st.executeQuery();
			
			if(rs.next()) {
				String title = rs.getString("TITLE");
				String writerId = rs.getString("WRITER_ID");
				String content = rs.getString("CONTENT");
				Timestamp regDate = rs.getTimestamp("REGDATE");
				int hit = rs.getInt("HIT");
				String files = rs.getString("FILES");
				boolean pub = rs.getBoolean("PUB");
				
				notice = new Notice(id, title, writerId, content, regDate, hit, files, pub);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) {
					rs.close();
					rs = null;
				}
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return notice;
	}
	
	public Notice getNextPubNotice(int id) {
		Notice notice = null;
		
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			conn = dataFactory.getConnection();
			
			String sql = "SELECT * FROM notice "
					+ "WHERE "
					+ "PUB "
					+ "REGDATE > (SELECT REGDATE FROM notice WHERE ID=?) "
					+ "ORDER BY REGDATE ASC LIMIT 1";
			st = conn.prepareStatement(sql);
			st.setInt(1, id);
			rs = st.executeQuery();
			
			if(rs.next()) {
				String title = rs.getString("TITLE");
				String writerId = rs.getString("WRITER_ID");
				String content = rs.getString("CONTENT");
				Timestamp regDate = rs.getTimestamp("REGDATE");
				int hit = rs.getInt("HIT");
				String files = rs.getString("FILES");
				boolean pub = rs.getBoolean("PUB");
				
				notice = new Notice(id, title, writerId, content, regDate, hit, files, pub);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) {
					rs.close();
					rs = null;
				}
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return notice;
	}
	
	public List<NoticeCommentCountView> getNoticeList() {
		return getNoticeList(1, null, null);
	}
	
	public List<NoticeCommentCountView> getNoticeList(int page) {
		return getNoticeList(page, null, null);
	}
	
	public List<NoticeCommentCountView> getNoticeList(String field, String query) {
		return getNoticeList(1, field, query);
	}
	
	public List<NoticeCommentCountView> getNoticeList(int page, String field, String query) {
		List<NoticeCommentCountView> notices = new ArrayList<NoticeCommentCountView>();
		
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			conn = dataFactory.getConnection();
			
			if(page < 1) {
				page = 1;
			}
			int offset = (page - 1) * COUNT_OF_NOTICE_ON_PAGE;
			
			switch(field == null ? "" : field) {
				case "title":
				case "content":
				case "writer_id":
				{
					String sql = "SELECT * "
							+ "FROM notice_comment_count_view "
							+ "WHERE "
							+ field + " LIKE ? "
							+ "ORDER BY REGDATE DESC "
							+ "LIMIT ?, ?";
					st = conn.prepareStatement(sql);
					st.setString(1, "%" + query + "%");
					st.setInt(2, offset);
					st.setInt(3, COUNT_OF_NOTICE_ON_PAGE);
					break;
				}
				default:
				{
					String sql = "SELECT * "
							+ "FROM notice_comment_count_view "
							+ "ORDER BY REGDATE DESC "
							+ "LIMIT ?, ?";
					st = conn.prepareStatement(sql);
					st.setInt(1, offset);
					st.setInt(2, COUNT_OF_NOTICE_ON_PAGE);
					break;
				}
			}
			rs = st.executeQuery();
			
			while(rs.next()) {
				int id = rs.getInt("ID");
				String title = rs.getString("TITLE");
				String writerId = rs.getString("WRITER_ID");
				Timestamp regDate = rs.getTimestamp("REGDATE");
				int hit = rs.getInt("HIT");
				String files = rs.getString("FILES");
				boolean pub = rs.getBoolean("PUB");
				int commentCount = rs.getInt("COMMENT_COUNT");
				
				notices.add(new NoticeCommentCountView(id, title, writerId, regDate, hit, files, pub, commentCount));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) {
					rs.close();
					rs = null;
				}
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return notices;
	}
	
	public int getNoticeCount(String field, String query) {
		int result = 0;
		
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			conn = dataFactory.getConnection();
			
			switch(field == null ? "" : field) {
				case "title":
				case "content":
				{
					String sql = "SELECT COUNT(*) FROM notice "
							+ "WHERE "
							+ field + " LIKE ?";
					st = conn.prepareStatement(sql);
					st.setString(1, "%" + query + "%");
					break;
				}
				default:
				{
					String sql = "SELECT COUNT(*) FROM notice";
					st = conn.prepareStatement(sql);
					break;
				}
			}
			rs = st.executeQuery();
			
			if(rs.next()) {
				int count = rs.getInt(1);
				
				result = count;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) {
					rs.close();
					rs = null;
				}
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	public int getNoticeCount() {
		return getNoticeCount(null, null);
	}
	
	public Notice getNotice(int id) {
		Notice notice = null;
		
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			conn = dataFactory.getConnection();
			
			String sql = "SELECT * FROM notice WHERE ID=?";
			st = conn.prepareStatement(sql);
			st.setInt(1, id);
			rs = st.executeQuery();
			
			if(rs.next()) {
				String title = rs.getString("TITLE");
				String writerId = rs.getString("WRITER_ID");
				String content = rs.getString("CONTENT");
				Timestamp regDate = rs.getTimestamp("REGDATE");
				int hit = rs.getInt("HIT");
				String files = rs.getString("FILES");
				boolean pub = rs.getBoolean("PUB");
				
				notice = new Notice(id, title, writerId, content, regDate, hit, files, pub);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) {
					rs.close();
					rs = null;
				}
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return notice;
	}

	public Notice getPrevNotice(int id) {
		Notice notice = null;
		
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			conn = dataFactory.getConnection();
			
			String sql = "SELECT * FROM notice "
					+ "WHERE "
					+ "REGDATE < (SELECT REGDATE FROM notice WHERE ID=?) "
					+ "ORDER BY REGDATE DESC LIMIT 1";
			st = conn.prepareStatement(sql);
			st.setInt(1, id);
			rs = st.executeQuery();
			
			if(rs.next()) {
				String title = rs.getString("TITLE");
				String writerId = rs.getString("WRITER_ID");
				String content = rs.getString("CONTENT");
				Timestamp regDate = rs.getTimestamp("REGDATE");
				int hit = rs.getInt("HIT");
				String files = rs.getString("FILES");
				boolean pub = rs.getBoolean("PUB");
				
				notice = new Notice(id, title, writerId, content, regDate, hit, files, pub);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) {
					rs.close();
					rs = null;
				}
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return notice;
	}
	
	public Notice getNextNotice(int id) {
		Notice notice = null;
		
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			conn = dataFactory.getConnection();
			
			String sql = "SELECT * FROM notice "
					+ "WHERE "
					+ "REGDATE > (SELECT REGDATE FROM notice WHERE ID=?) "
					+ "ORDER BY REGDATE ASC LIMIT 1";
			st = conn.prepareStatement(sql);
			st.setInt(1, id);
			rs = st.executeQuery();
			
			if(rs.next()) {
				String title = rs.getString("TITLE");
				String writerId = rs.getString("WRITER_ID");
				String content = rs.getString("CONTENT");
				Timestamp regDate = rs.getTimestamp("REGDATE");
				int hit = rs.getInt("HIT");
				String files = rs.getString("FILES");
				boolean pub = rs.getBoolean("PUB");
				
				notice = new Notice(id, title, writerId, content, regDate, hit, files, pub);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) {
					rs.close();
					rs = null;
				}
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return notice;
	}

	public int update(Notice notice) {
		int result = -1;
		
		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = dataFactory.getConnection();
			
			int id = notice.getId();
			String title = notice.getTitle();
			String content = notice.getContent();
			String files = notice.getFiles();
			
			String sql = "UPDATE notice "
					+ "SET "
					+ "TITLE=?, "
					+ "CONTENT=?, "
					+ "FILES=? "
					+ "WHERE "
					+ "ID=?";
			
			// ?로 나중에 값을 넣을 때는 prepareStatement를 사용하면 된다.
			st = conn.prepareStatement(sql);
			st.setString(1, title);
			st.setString(2, content);
			st.setString(3, files);
			st.setInt(4, id);
			
			// 변경된 row의 수를 반환한다.
			result = st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	public int delete(int id) {
		int result = -1;
		
		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = dataFactory.getConnection();
			
			String sql = "DELETE notice "
					+ "WHERE "
					+ "ID=?";
			
			// ?로 나중에 값을 넣을 때는 prepareStatement를 사용하면 된다.
			st = conn.prepareStatement(sql);
			st.setInt(1, id);
			
			// 변경된 row의 수를 반환한다.
			result = st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
}
